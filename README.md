# :chart_with_upwards_trend: Telemetry GUI

Utilizes OpenMCT (a NASA web framework) in order to monitor all of the measurements that we receive from the sensors in real time.

### :file_folder: Structure
```
static-test-visual-data/
├── public/
│   ├── assets/
│   └── index.html
├── src/
│   ├── displays/
│   │   └── static_test_display_layout.json
│   ├── lib/
│   │   └── http.js
│   ├── server/
│   │   ├── server.js
│   │   ├── static-server.js
│   │   ├── realtime-server.js
│   │   ├── history-server.js
│   │   └── staticTest.js
│   ├── realtime-telemetry-plugin.js
│   ├── historical-telemetry-plugin.js
│   ├── dictionary-plugin.js
│   └── dictionary.json
├── package.json
├── webpack.config.js
├── .gitignore
├── .npmignore
└── README.md
```
### After installing nvm
- `npv install 12.21.0`
- `nvm use 12.21.0`
### And then continue

### :nut_and_bolt: Setup
**Prerequisites**
- [node.js](https://nodejs.org/en/) (12.21.0)
- [git](https://git-scm.com/)


Use the terminal for the following steps:
```bash
git clone https://gitlab.com/librespacefoundation/cronos-rocket/avionics/ground-station/static-test-visual-data.git
cd static-test-visual-data
npm install
```

###  :joystick: Usage

Use the terminal inside the ```static-test-visual-data/``` folder to start the deployment server using the following command:
```bash
npm start
```
and click [here](http://127.0.0.1:8080/) to access it.

### Setting up an NGINX web server on a Raspberry Pi
```bash
cd .docker
docker-compose up --build -d
```
