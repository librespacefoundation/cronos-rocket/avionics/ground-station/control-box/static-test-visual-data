var express = require('express');
var path = require('path');


function StaticServer() {
    var router = express.Router();

    router.use(
      '/assets',
      express.static(path.join(__dirname + '/../../public/assets/'))
    );

    router.use(
      '/openmctAssets',
      express.static(path.join(__dirname + '/../../node_modules/openmct/dist/'))
    );

    router.use(
      '/',
      express.static(path.join(__dirname + '/../..'))
    );

    router.use(express.static(__dirname + '/../../public'));

    return router
}

module.exports = StaticServer;
