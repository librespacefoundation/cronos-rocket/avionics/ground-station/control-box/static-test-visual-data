var StaticTest = require('./staticTest');
var RealtimeServer = require('./realtime-server');
var HistoryServer = require('./history-server');
var StaticServer = require('./static-server');

var expressWs = require('express-ws');
var app = require('express')();
expressWs(app);

var staticTest = new StaticTest();
var realtimeServer = new RealtimeServer(staticTest);
var historyServer = new HistoryServer(staticTest);
var staticServer = new StaticServer();

app.use('/realtime', realtimeServer);
app.use('/history', historyServer);
app.use('/', staticServer);

var PORT = process.env.PORT || 8080;
var HOST = '0.0.0.0';

app.listen(PORT, HOST, function () {
    console.log('Open MCT hosted at http://192.168.4.1:' + PORT);
    console.log('History hosted at http://192.168.4.1:' + PORT + '/history');
    console.log('Realtime hosted at ws://192.168.4.1:' + PORT + '/realtime');
});
